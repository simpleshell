/******************************************************************************
 * The MIT License
 *
 * Copyright (c) 2010 Stefan 'psYchotic' Zwanenburg
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#define _GNU_SOURCE
#include "builtins.h"
#include <curses.h>
#include <errno.h>
#include "mystring.h"
#include <pwd.h>
#include <stdio.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

/**
 * Returns true if the argument is an empty SH_BUILTIN struct.
 */
#define IS_LAST_BUILTIN(builtin) ((builtin).name == NULL && \
                                  (builtin).command == NULL && \
                                  (builtin).help == NULL)
/**
 * Returns true if last_status indicates that the shell is done reading input.
 */
#define IS_DONE ((last_status >> 8) & 1)
/**
 * Returns true if last_status indicates that the shell should exit.
 */
#define IS_EXIT ((last_status >> 9) & 1)

static const struct SH_BUILTIN builtins[] = {
    {"cd", cd_cmd, "Usage: cd <DIR>\nChanges the current working directory to DIR.\n"},
    {"exit", exit_cmd, "Usage: exit\nExits this shell.\n"},
    {"help", help_cmd, "Usage: help [BUILTIN]\nShows how to use BUILTIN.\nIf no BUILTIN was given, shows a list of builtins.\n"},
    {"kill", kill_cmd, "Usage: kill <PID> ...\nSends the SIGTERM signal to all the PIDs.\n"},
    {"status", status_cmd, "Usage: status\nPrints the exit status of the last command.\n"},
    {":", true_cmd, "Usage: :\nDoes absolutely nothing.\n"},
    {".", source_cmd, "Usage: . <FILENAME>\nReads a file and executes whatever is inside it.\n"},
    {NULL, NULL, NULL}
};

static int last_status = 0;

/**
 * Counts the number of tokens in a string.
 * Note that tokens are delimited by spaces, unless the space is escaped.
 * @param input the string to check for tokens
 * @return the number of tokens in the input string
 */
int count_tokens(char *input) {
    int tokens = 0;
    while (*input == ' ') ++input;
    if (*input == '\0' || *input == '#') {
        return tokens;
    } else {
        tokens++;
        for (; *input != '\0'; ++input) {
            if (*input == '\\' && *(input + 1) == ' ') {
                ++input;
            } else if (*input == ' ') {
                for (; *(input + 1) == ' '; ++input);
                if (*(input + 1) != '\0' && *(input + 1) != '#') {
                    tokens++;
                }
            } else if (*input == '#') {
                break;
            }
        }
    }

    return tokens;
}

/**
 * Splits the input into an array tokens.
 * Also implements a simple mechanism for escaping spaces. You basically use a
 * single backslash to escape a space (or another backslash). In the event that
 * a backslash is followed by a character other than a space or a backslash, it
 * is interpreted as-is.\n
 * Note that the allocated memory for the returned array
 * may exceed what is required in order to store all words.
 * This does not in any way pose a problem.\n
 * Every element in the returned array (before the terminating NULL)
 * should be freed, along with the array itself.
 * @param input the string to be split into words
 * @return an array of words
 */
char **split_input(char *input) {
    int tokens = count_tokens(input);
    int i = 0, j = 0;
    char *single_word = malloc(sizeof(char) * strlen(input) + 1);
    if (single_word == NULL) {
        fprintf(stderr, "Could not allocate memory: %s\nExiting...\n", strerror(errno));
        exit(EXIT_FAILURE);
    }
    char **split = malloc(sizeof(char *) * (tokens+1));
    if (split == NULL) {
        fprintf(stderr, "Could not allocate memory: %s\nExiting...\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

    /* skip first spaces */
    while (*input == ' ') ++input;
    for (; *input != '\0'; input++) {
        if (*input == '\\' && (
                    *(input + 1) == ' ' ||
                    *(input + 1) == '\\' ||
                    *(input + 1) == '#'
                    )) {
                single_word[i++] = *(++input);
        } else if (*input == ' ') {
            single_word[i] = '\0';
            split[j++] = strdup(single_word);
            for (; *(input+1) == ' '; ++input);
            i = 0;
        } else if (*input == '#') {
            break;
        } else {
            single_word[i++] = *input;
        }
    }

    if (i != 0) {
        single_word[i] = '\0';
        split[j++] = strdup(single_word);
    }
        
    split[j] = NULL;

    free(single_word);

    return split;
}

/**
 * Prints help about this shell or one of its builtins.
 * @return always zero
 */
int help_cmd(char **argv) {
    int i;
    if (argv[0] == NULL) {
        printf("Available builtins:\n");
        for (i = 0; !IS_LAST_BUILTIN(builtins[i]); i++) {
            printf("  %s\n", builtins[i].name);
        }
        printf("Type 'help <BUILTIN>' for more information on BUILTIN.\n");
    } else if (argv[1] != NULL) {
        fprintf(stderr, "The 'help' builtin takes at most one argument. See 'help help'.\n");
    } else {
        for (i = 0; !IS_LAST_BUILTIN(builtins[i]); i++) {
            if (!strcmp(argv[0], builtins[i].name)) {
                printf("%s", builtins[i].help);
                break;
            }
        }
        if (IS_LAST_BUILTIN(builtins[i])) {
            printf("Unknown builtin '%s'. Type 'help' to see a list of builtins.\n", argv[0]);
        }
    }
    return 0;
}

/**
 * Exits the shell, unless an incorrect argument or too many arguments specified.
 * @return zero if no argument was specified,
 * the argument & 0xFF (see man 3 exit for details) if specified,
 * one if there were more than one arguments or if the one argument could not
 * be parsed, in which two cases no actual exiting occurs.
 */
int exit_cmd(char **argv) {
    int exit_status;
    int read_chars;
    if (argv[0] == NULL) {
        return (3 << 8);
    } else if (argv[0] != NULL && argv[1] == NULL) {
        if (!sscanf(argv[0], "%d%n", &exit_status, &read_chars) || read_chars != strlen(argv[0])) {
            fprintf(stderr, "Could not parse the argument ('%s') as an integer.\n", argv[0]);
            return 1;
        } else {
            return (3 << 8) | (exit_status & 0xFF);
        }
    } else {
        fprintf(stderr, "The 'exit' builtin takes at most one argument. See 'help exit' for details.\n");
        return 1;
    }
}

/**
 * Sends a signal to a list of PIDs.
 * In this implementation, the SIGTERM (15) signal is sent. If an error
 * occurs whilst trying to send said signal, a message is printed on stderr.
 * Inversely, when sending the signal succeeds, a message is printed on stdout
 * to notify the user.
 * @return always zero
 */
int kill_cmd(char **argv) {
    int i;
    int read_chars;
    pid_t pid;
    if (argv[0] == NULL) {
        printf("The 'kill' builtin takes at least one argument (a PID).\n");
    } else {
        for (i = 0; argv[i] != NULL; i++) {
            if (sscanf(argv[i], "%d%n", &pid, &read_chars) && read_chars == strlen(argv[i])) {
                if (kill(pid, SIGTERM)) {
                    fprintf(stderr, "Couldn't kill the process with PID %d: %s\n", pid, strerror(errno));
                } else {
                    fprintf(stderr, "Killed (or sent signal %d) to PID %d.\n", SIGTERM, pid);
                }
            } else {
                printf("%s is not a valid PID. Skipping.\n", argv[i]);
            }
        }
    }

    return 0;
}

/**
 * Does nothing, and always succeeds, even if it has arguments.
 * Note that the arguments *may* be executed if the shell implements some
 * sort of expansion. Use '#' to create comments.
 * @return always zero
 */
int true_cmd(char **argv) {
    return 0;
}

/**
 * Prints the exit status of the last command.
 * @return nonzero if an argument was specified (in which case it doesn't
 * actually print the last exit status), zero otherwise.
 */
int status_cmd(char **argv) {
    if (argv[0] != NULL) {
        fprintf(stderr, "The 'status' builtin doesn't take any arguments.");
        return 1;
    } else {
        printf("%d\n", last_status);
    }

    return 0;
}

/**
 * Changes the current working directory.
 * Note that the actual changing of the current working directory may fail,
 * in which case an error is printed on stderr. This command takes a single
 * argument: the directory to change to.
 * @return always zero
 */
int cd_cmd(char **argv) {
    char *todir;
    if (argv[0] == NULL) {
        struct passwd *passwd = getpwuid(getuid());
        if (passwd == NULL) {
            fprintf(stderr, "Could not get the current user's home directory: %s\n", strerror(errno));
        } else {
            todir = passwd->pw_dir;
        }
    } else if (argv[1] == NULL) {
        todir = argv[0];
    } else {
        fprintf(stderr, "The 'cd' builtin takes at most one argument (the directory to move to).\n");
        return 0;
    }

    if (chdir(todir)) {
        fprintf(stderr, "Could not change the current working directory to '%s': %s.\n", todir, strerror(errno));
    }

    return 0;
}

/**
 * Handles a line of input.
 * This function handles a line of input, which may
 * result in two things:
 *  - a builtin command is executed
 *  - the process forks and launches the application
 *      specified in the input string
 * @param input a string representing a single line of input
 * @param interactive if nonzero, prints "exit" followed by a newline if input
 * equals NULL, otherwise doesn't print anything.
 */
void handle_input(char *input, int interactive) {
    pid_t pid;
    char **argv;
    int wait_status;
    int i;
    if (input == NULL) {
        if (interactive) {
            printf("exit\n");
            last_status = (1 << 8);
        } else {
            last_status = (1 << 8) | (last_status & 0xFF);
        }
    } else if (input[0] == '\0') {
        /* do nothing */
        free(input);
    } else {
        argv = split_input(input);
        if (*argv != NULL) {
            if (history_length == 0 || history_search_pos(input, 1, 0) == -1) {
                add_history(input);
            }
            free(input);
            for (i = 0; !IS_LAST_BUILTIN(builtins[i]); i++) {
                if (!strcmp(argv[0], builtins[i].name)) {
                    last_status = builtins[i].command(&argv[1]);
                    break;
                }
            }
            if (IS_LAST_BUILTIN(builtins[i])) {
                pid = fork();
                if (pid == 0) {
                    if (execvp(argv[0], argv)) {
                        fprintf(stderr, "Could not execute '%s': %s\n", argv[0], strerror(errno));
                    }
                    last_status = (1 << 8);
                } else {
                    while (!waitpid(pid, &wait_status, 0) && WIFEXITED(wait_status));
                    last_status = WEXITSTATUS(wait_status);
                }
            }
            for (i = 0; argv[i] != NULL; i++) {
                free(argv[i]);
            }
        }
        free(argv);
    }
}

/**
 * Executes the contents of the argument file in the current shell.
 * Note that the 'exit' builtin within these files does not currently
 * actually exit the current shell, it merely stops the parsing of the file.
 * @return 
 */
int source_cmd(char **argv) {
    FILE *input;
    MString line;
    if (argv[0] == NULL || argv[1] != NULL) {
        fprintf(stderr, "The '.' builtin takes exactly one argument. See 'help .' for details.\n");
    } else {
        input = fopen(argv[0], "r");
        if (input == NULL) {
            fprintf(stderr, "Could not open '%s' for sourcing: %s\n", argv[0], strerror(errno));
        } else {
            do {
                line = mystring_new(50, 50);
                if (mystring_getline(line, input) != -1) {
                    handle_input(line->str, 0);
                    free(line);
                } else {
                    mystring_free(line);
                    handle_input(NULL, 0);
                }
            } while (!IS_DONE);
            fclose(input);
        }
    }

    if (IS_EXIT) {
        return last_status;
    } else {
        return last_status & 0xFF;
    }
}

/**
 * Reads a line of input from stdin.
 * This function is mainly here for modularity,
 * meaning that if I need or want to change the way
 * I get input, I would do it here once.
 * @param prompt the prompt string to display
 * @return a string containing a line of input
 */
char *get_input(char *prompt) {
    char *line;
    char *cwd;
    char *local_prompt;

    if (prompt == NULL) {
        cwd = get_current_dir_name();
        if (cwd == NULL) {
            cwd = "?";
        }

        struct passwd *passwd = getpwuid(getuid());
        char *username = "?";
        if (passwd != NULL) {
            username = passwd->pw_name;
        }

        local_prompt = malloc(sizeof(char) * (strlen(cwd) + strlen(username) + 4));
        if (local_prompt == NULL) {
            fprintf(stderr, "Could not allocate memory: %s\nExiting...\n", strerror(errno));
            exit(EXIT_FAILURE);
        }
        local_prompt[0] = '\0';
        strcat(local_prompt, username);
        strcat(local_prompt, "@");
        if (passwd != NULL && strstr(cwd, passwd->pw_dir) != NULL) {
            strcat(local_prompt, "~");
            strcat(local_prompt, cwd+strlen(passwd->pw_dir));
        } else {
            strcat(local_prompt, cwd);
        }
        strcat(local_prompt, "$ ");
    } else {
        local_prompt = prompt;
    }

    line = readline(local_prompt);

    if (prompt == NULL) {
        if (strcmp(cwd, "?")) {
            free(cwd);
        }
        free(local_prompt);
    }

    return line;
}

/**
 * Prints help.
 * @param progname the name of the current program.
 */
void print_usage(char *progname) {
    printf("Usage: %s [OPTIONS]\n", progname);
    printf("  Options:\n");
    printf("    -p <prompt>     The prompt to be displayed\n");
    printf("    -h              Display this help\n");
}

/**
 * Handle SIGINT.
 * Discards the current readline input and redisplays the prompt.
 */
void handle_interrupt(int signal) {
    printf("\n");
    rl_on_new_line();
    rl_replace_line("", 1);
    reset_prog_mode();
    rl_redisplay();
}

int main(int argc, char **argv) {
    char *prompt = NULL;
    char *input;
    char option;
    while ((option = getopt(argc, argv, ":hp:")) != -1) {
        switch(option) {
            case 'h':
                print_usage(argv[0]);
                exit(EXIT_SUCCESS);
                break;
            case 'p':
                prompt = optarg;
                break;
            case ':':
                printf("Missing argument for option '%c'.\n", optopt);
                print_usage(argv[0]);
                exit(EXIT_FAILURE);
                break;
            case '?':
                printf("Unknown option '%c'.\n", optopt);
                print_usage(argv[0]);
                exit(EXIT_FAILURE);
            default:
                print_usage(argv[0]);
                exit(EXIT_SUCCESS);
                break;
        }
    }

    sigset_t sigset;
    struct sigaction action;
    if (sigemptyset(&sigset)) {
        fprintf(stderr, "Could not install a signal handler for SIGINT.\n");
    } else {
        action.sa_handler = handle_interrupt;
        action.sa_mask = sigset;
        action.sa_flags = 0;
        sigaction(SIGINT, &action, NULL);
    }
    using_history();
    rl_catch_signals = 0;

    do {
        input = get_input(prompt);
        handle_input(input, 1);
    } while (!IS_DONE);

    return last_status & 0xFF;
}
