#ifndef _BUILTINS_H
#define _BUILTINS_H
struct SH_BUILTIN {
    char *name;
    int (*command)(char **);
    char *help;
};

int cd_cmd(char **argv);
int exit_cmd(char **argv);
int help_cmd(char **argv);
int kill_cmd(char **argv);
int source_cmd(char **argv);
int status_cmd(char **argv);
int true_cmd(char **argv);
#endif
