PROJ=ss
CFLAGS+=-Wall -Werror
LDFLAGS+=-lreadline
DESTDIR=/usr/bin/

OBJS=mystring.o ss.o

all: $(PROJ)

$(PROJ): $(OBJS)
	$(CC) -o $(PROJ) $(OBJS) $(CFLAGS) $(LDFLAGS)

clean:
	rm -rf $(OBJS) $(PROJ)
	rm -rf doc

doc:
	doxygen

install: $(PROJ)
	install -d $(DESTDIR)
	install -m755 $(PROJ) $(DESTDIR)
